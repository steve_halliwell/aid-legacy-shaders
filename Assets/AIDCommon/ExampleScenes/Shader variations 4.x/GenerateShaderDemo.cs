﻿using UnityEngine;
using System.Collections;

public class GenerateShaderDemo : MonoBehaviour {

	public Material[] mats;
	public GameObject prefab;
	public float step;
	public Vector2 xlims,ylims;
    public float spd = 10;
    
    public Light point, dir;

	// Use this for initialization
	void Start () {
		for(int i = 0 ; i < mats.Length; i++)
		{
			(Instantiate(prefab, Vector3.right * i * step, Quaternion.identity) as GameObject).GetComponentInChildren<SetShaderToChildren>().mat = mats[i];
		}
		
		xlims.x = 0;
		xlims.y = step * (mats.Length-1);
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 tmp = transform.position + new Vector3(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical"),0) * Time.deltaTime * spd;
		tmp.x = Mathf.Clamp(tmp.x, xlims.x, xlims.y);
		tmp.y = Mathf.Clamp(tmp.y, ylims.x, ylims.y);
		
		transform.position = tmp;	
		tmp.z = 0;
		tmp.y = 0;
		transform.LookAt(tmp);
    }
    
    void OnGUI()
    {
   		GUILayout.BeginArea(new Rect(5,5,200,200));
    
    	GUILayout.BeginVertical();
    	
    	GUILayout.BeginHorizontal();
    	GUILayout.Label("Point Intensity; ");
        point.intensity = GUILayout.HorizontalSlider(point.intensity,0,4);
        GUILayout.EndHorizontal();
		
		GUILayout.BeginHorizontal();
		GUILayout.Label("Dir Intensity; ");
		dir.intensity = GUILayout.HorizontalSlider(dir.intensity,0,2);
		GUILayout.EndHorizontal();
        
        GUILayout.EndVertical();
        
        GUILayout.EndArea();
	}
}
