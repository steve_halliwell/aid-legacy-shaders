﻿using UnityEngine;
using System.Collections;

public class SetShaderToChildren : MonoBehaviour {

	public Material mat;
	public int tile = 1;
    public float shine = 0.7f;
	public TextMesh txtmsh;

	// Use this for initialization
	void Start () {
	
		txtmsh.text = mat.name;
	
		mat.SetTextureScale("_MainTex", new Vector2(tile,tile));
		mat.SetTextureScale("_BumpMap", new Vector2(tile,tile));
        mat.SetFloat("Shininess",shine);
		
		foreach(MeshRenderer r in GetComponentsInChildren<MeshRenderer>())
        {
            if(r.GetComponent<TextMesh>() == null)
			    r.material = mat;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
