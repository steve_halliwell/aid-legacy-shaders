﻿#ifndef NON_TRANS_DIF_SPEC_HEADER
#define NON_TRANS_DIF_SPEC_HEADER

/*
	We have many simple variations on the standard bump spec set up so extracting it out means its easier to update all later
*/

sampler2D _MainTex;
sampler2D _BumpMap;
fixed4 _Color;
float _Shininess;

struct Input {
	float2 uv_MainTex;
	float2 uv_BumpMap;
};

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
	o.Albedo = tex.rgb * _Color.rgb;
	o.Gloss = tex.a;
	//o.Alpha = 1;
	o.Specular = _Shininess;
	o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
}
inline float4 LightingSurfaceGSpec (SurfaceOutput s, float3 lightDir, float3 viewDir, float atten)
{
	half3 h = normalize (lightDir + viewDir);
	
	float diff = dot (s.Normal, normalize(lightDir));

#endif//NON_TRANS_DIF_SPEC_HEADER