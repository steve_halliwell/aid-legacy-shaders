#ifndef NON_TRANS_DIF_SPEC_FOOTER
#define NON_TRANS_DIF_SPEC_FOOTER


	float nh = max (0, dot (s.Normal, h));
	//float ang = acos(dot(h,s.Normal));
	float spec = pow (nh, s.Specular*128.0) * s.Gloss;
	//float spec = exp(-pow(ang/s.Specular,2)) * s.Gloss;
	
	float3 diffuseRes = s.Albedo * _LightColor0.rgb * diff;
	float3 specRes = _LightColor0.rgb * _SpecColor.rgb * spec;
	
	fixed4 c;
	c.rgb = (diffuseRes + specRes) * (atten * 2);
	//c.a = s.Alpha + _LightColor0.a * _SpecColor.a * spec * atten;
	return c;
}

#endif//NON_TRANS_DIF_SPEC_FOOTER