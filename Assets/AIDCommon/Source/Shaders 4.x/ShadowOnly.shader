﻿//http://answers.unity3d.com/questions/269292/having-an-invisible-object-that-casts-shadows.html
Shader "Custom/ShadowOnly" {
     Subshader
     {
         UsePass "VertexLit/SHADOWCOLLECTOR"    
         UsePass "VertexLit/SHADOWCASTER"
     }
 
     Fallback off
 }