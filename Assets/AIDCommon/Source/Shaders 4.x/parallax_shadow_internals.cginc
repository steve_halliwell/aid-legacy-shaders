﻿#define MIN_PARA_LINEAR_SHADOW_SAMPLES 20
#define MAX_PARA_LINEAR_SHADOW_SAMPLES 100
#define PARA_SHADOW_STARTIING_OFFSET 0.05f
#define MIN_PARA_SHADOW_STARTIING_OFFSET (1.0/256.0f + 1.e-5)
#define PARA_SHADOW_DYDX_SCALE 1.0f
#define PARA_SHADOW_BLUR_DYDX_SCALE 10.0f
#define PARA_SHADOW_MIN_STEP_SIZE 0.002f
#define PARA_SHADOW_GAUSSIAN_SCATTER_SCALE 1.5f

//TODO
// could still use surrounding samples to soften hard edges

#if !defined(_OFFSET_OCCULUSION_NONE)
#if defined(_PARA_OFFSET)
	//http://dev.theomader.com/gaussian-kernel-calculator/
	//const float weights[3] = {0.27901, 0.44198, 0.27901};
	//don't self shadow backfacing
	if(diff > 0)
	{
		float2 offset = s.UV;
		//rate of changed used in tex2dgrad and to access different mip levels as tex2dLod is not completely supported
		float2 dyRaw = ddy( offset ), dxRaw = ddx( offset );
		
		float dy = dyRaw, dx = dxRaw;
		#if defined(_OFFSET_OCCULUSION_MIP)
		dy *= PARA_SHADOW_DYDX_SCALE;
		dx *= PARA_SHADOW_DYDX_SCALE;
		#endif
		//float2 offset = float2(0,0);// ParallaxOffset (h, _Parallax, IN.viewDir);
		float bumpScale = _Parallax;
		
		float3 tsL = normalize(lightDir);
		
	    //we could use real height from previous calcs but we are in lower texture mips and that is introducting acne
	    float heightSample = tex2Dgrad(_AOHMap, offset,dx,dy).a;
	    float height = heightSample;
	    //we have a starting height it needs to be a TINY bit above to avoid shadow acne
	    height += MIN_PARA_SHADOW_STARTIING_OFFSET;
		
		//also be aware of how much height there is to check, as well as the angle we are scanning at
	    int numShadowSteps = (1.0f-height) * lerp(MAX_PARA_LINEAR_SHADOW_SAMPLES * _OcculusionQualityScale,MIN_PARA_LINEAR_SHADOW_SAMPLES * _OcculusionQualityScale, tsL.z);
	    float step = min((1.0f-height) / numShadowSteps, PARA_SHADOW_MIN_STEP_SIZE);
	    
	 
	 	//uv space movement per step
	    float2 delta = float2(-tsL.x, -tsL.y) * bumpScale / (tsL.z * numShadowSteps);
	    
	  	//moves linearly thro height field until we hit a peak
	    for ( int i = 0; i < numShadowSteps; i++) {
	        offset -= delta;
	        height += step;
	        heightSample = tex2Dgrad(_AOHMap, offset,dx,dy).a;
	        
	        if(height >= 1)
	        	break;
	        
	        if (heightSample > height) 
	        {
	       		#if defined(_OFFSET_OCCULUSION_FLAT)
		        paraShadow = 1;
		        break;
	        	#endif
	        	//blur by neighbour results
	        	//TODO needs to be rotated and offset not aligned north south in uv space
	        	#if defined(_OFFSET_OCCULUSION_SMOOTH)
	        	float2 delta90Rot = delta * PARA_SHADOW_GAUSSIAN_SCATTER_SCALE;
	        	delta90Rot = float2(delta90Rot.y,-delta90Rot.x);
	        	
	        	float upper = tex2Dgrad(_AOHMap, offset + delta90Rot,dx,dy).a;
	        	float lower = tex2Dgrad(_AOHMap, offset - delta90Rot,dx,dy).a;
	        	float gatheredHighResHeight = heightSample * 0.44198 + upper * 0.27901 + lower * 0.27901;
	        	#else
	        	float gatheredHighResHeight = heightSample;
	        	#endif
	        	
//	        	//sample lower mip and blend in diff to smooth edges of shadow
	        	float lowerMipheightSampleC = tex2Dgrad(_AOHMap, offset, dxRaw*PARA_SHADOW_BLUR_DYDX_SCALE, dyRaw*PARA_SHADOW_BLUR_DYDX_SCALE).a;
	        	//quick hack to check usefullness
	        	//float4 lowerMipheightSampleU = tex2Dgrad(_AOHMap, offset + delta90Rot, dxRaw*PARA_SHADOW_BLUR_DYDX_SCALE, dyRaw*PARA_SHADOW_BLUR_DYDX_SCALE);
	        	//float4 lowerMipheightSampleD = tex2Dgrad(_AOHMap, offset - delta90Rot, dxRaw*PARA_SHADOW_BLUR_DYDX_SCALE, dyRaw*PARA_SHADOW_BLUR_DYDX_SCALE);
	        	
	        	float gatheredLowResHeight = lowerMipheightSampleC;//0.27901 * lowerMipheightSampleU.a + 0.27901 * lowerMipheightSampleD.a + 0.44198 * lowerMipheightSampleC.a;
	        	
	        	paraShadow = 1 + (gatheredLowResHeight - gatheredHighResHeight)/(1-height);
	        	//paraShadow = max(paraShadow,0);
	        	break;
	        }
	    }
    }
#endif
#endif