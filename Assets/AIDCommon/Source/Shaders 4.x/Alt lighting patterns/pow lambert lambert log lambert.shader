﻿Shader "AID/Bumped Specular - Diffuse Pow self log" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
	_Shininess ("Shininess", Range (0.03, 100)) = 0.078125
	_DiffusePower ("DiffusePower", Float) = 1.0
	_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
	_BumpMap ("Normalmap", 2D) = "bump" {}
}
SubShader { 
	Tags { "RenderType"="Opaque" }
	LOD 400
	
CGPROGRAM
#pragma surface surf SurfaceGSpec

float _DiffusePower;

#include "../NonTransDifSpecHeader.cginc"

	diff = pow(diff,diff*log10(diff))-1;

#include "../NonTransDifSpecFooter.cginc"

ENDCG
}

FallBack "Bumped Specular"
}
