﻿Shader "AID/Bumped Specular - Diffuse Angle exp log" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
	_Shininess ("Shininess", Range (0.03, 100)) = 0.078125
	_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
	_BumpMap ("Normalmap", 2D) = "bump" {}
}
SubShader { 
	Tags { "RenderType"="Opaque" }
	LOD 400
	
CGPROGRAM
#pragma surface surf SurfaceGSpec

#define PI_H 1.570796327

#include "../NonTransDifSpecHeader.cginc"

	diff = acos(diff);
	//looks like a rotated s curve, that enters at 1 and exits at half pi
	diff = 1 - exp((diff+0.11)) * log10((diff+0.11)*.5) - 1.4;

#include "../NonTransDifSpecFooter.cginc"

ENDCG
}

FallBack "Bumped Specular"
}
