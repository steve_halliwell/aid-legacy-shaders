﻿Shader "AID/Bumped Specular" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
	_Shininess ("Shininess", Range (0.03, 100)) = 0.078125
	_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
	_BumpMap ("Normalmap", 2D) = "bump" {}
}
SubShader { 
	Tags { "RenderType"="Opaque" }
	LOD 400
	
CGPROGRAM

#pragma surface surf SurfaceGSpec

//no extra vars

#include "../NonTransDifSpecHeader.cginc"

	diff = max(0.00001,diff);

#include "../NonTransDifSpecFooter.cginc"


ENDCG
}

FallBack "Bumped Specular"
}
