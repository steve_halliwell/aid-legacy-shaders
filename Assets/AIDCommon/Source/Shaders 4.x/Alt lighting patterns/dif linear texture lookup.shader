﻿Shader "AID/Bumped Specular - Diffuse linear lookup" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
	_Shininess ("Shininess", Range (0.03, 100)) = 0.078125
	_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
	_BumpMap ("Normalmap", 2D) = "bump" {}
	_DifLightLookup ("Diffuse lighting lookup", 2D) = "white" {}
}
SubShader { 
	Tags { "RenderType"="Opaque" }
	LOD 400
	
CGPROGRAM
#pragma surface surf SurfaceGSpec

#define PI_H 1.570796327

sampler2D _DifLightLookup;

#include "../NonTransDifSpecHeader.cginc"

	//rerange lambert to linear 0-1
	diff = acos(diff)/PI_H;
	diff = tex2D(_DifLightLookup, half2(diff,0.5f));
	

#include "../NonTransDifSpecFooter.cginc"

ENDCG
}

FallBack "Bumped Specular"
}
