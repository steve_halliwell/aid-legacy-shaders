﻿Shader "AID/Bumped Specular Rerange" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
	_Shininess ("Shininess", Range (0.03, 100)) = 0.078125
	_DiffuseScale ("DiffuseScale", Float) = 0.5
	_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
	_BumpMap ("Normalmap", 2D) = "bump" {}
}
SubShader { 
	Tags { "RenderType"="Opaque" }
	LOD 400
	
CGPROGRAM
#pragma surface surf SurfaceGSpec

float _DiffuseScale;

#include "../NonTransDifSpecHeader.cginc"

	diff = (diff * _DiffuseScale) + (1.0 - _DiffuseScale);

#include "../NonTransDifSpecFooter.cginc"

ENDCG
}

FallBack "Bumped Specular"
}
