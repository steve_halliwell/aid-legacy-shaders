﻿Shader "Custom/AllTheMaps-defaultLighting" {
	Properties {
		
	_Color ("Main Color", Color) = (1,1,1,1)
	_MainTex ("Base (RGBA)", 2D) = "white" {}
	[Toggle] _VERTCOL ("Vert Colors?", Float) = 0
	_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
	_BumpMap ("Normalmap", 2D) = "bump" {}
	_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
	_SpecularityMap ("Specularity", 2D) = "grey" {}
//	_EmissMap ("Emissive", 2D) = "black" {}
//	_AOStrength ("Dir Ambient Occ Strength", Range (0.01, 3)) = 0.5
	_AOHMap ("AO(RGB) Height (A)", 2D) = "white" {}
	[KeywordEnum(None, Offset)] _PARA ("Parallax mode", Float) = 0
//	[KeywordEnum(None, Unity, Offset)] _PARA ("Parallax mode", Float) = 0
	_Parallax ("Height", Range (0.00001, 0.3)) = 0.05
//	[Toggle] _OFFSET_CLAMP ("Offset 0-1 clamp?", Float) = 0
	_OffsetQualityScale("Offset Quality scale", Range (0.1, 10)) = 1
//	[KeywordEnum(None, Flat, Mip, Smooth)] _OFFSET_OCCULUSION ("Offset Occulusion?", Float) = 0
//	_OcculusionShadowStrength ("Occ shadow strength", Range (0.01, 1)) = 0.5
//	_OcculusionQualityScale("Occ Quality scale", Range (0.1, 10)) = 1
//	
//	[Toggle] _LIGHTRAMPS ("Lighting Ramps?", Float) = 0
//	_DiffuseRamp ("Diffuse lighting lookup", 2D) = "white" {}
//	_SpecularRamp ("Specular lighting lookup", 2D) = "white" {}
	}
	SubShader {
		//Tags { "RenderType"="Transparent " }
		Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}
		//Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		//LOD 400
		
		CGPROGRAM
		#pragma surface surf BlinnPhong alphatest:_Cutoff
		#pragma target 3.0
		#pragma glsl
		//#pragma multi_compile _PARA_NONE _PARA_UNITY _PARA_OFFSET
		#pragma multi_compile _PARA_NONE _PARA_OFFSET
		//#pragma multi_compile _OFFSET_CLAMP_OFF _OFFSET_CLAMP_ON

//TODO
//reflect map?
//	gloss and reflect should become energy splits
//frenel
//triplanar noise to mix into height stretches
//cut off
//	disolve


		sampler2D _MainTex;
		sampler2D _BumpMap;
		sampler2D _AOHMap;
		sampler2D _SpecularityMap;
		fixed4 _Color;
		float _Parallax;
//		float _AOStrength;
//		float _OcculusionShadowStrength;
		float _OffsetQualityScale;
//		float _OcculusionQualityScale;
//		sampler2D _DiffuseRamp;
//		sampler2D _SpecularRamp;
		//sampler2D _EmissMap;

		struct Input {
			float2 uv_MainTex;
			float2 uv_BumpMap;
			float3 viewDir;
			#if defined(_VERTCOL_ON)
    		half4 color : COLOR0;
    		#endif
//			float3 customNorm;
//			float3 customView;
		};
		
		void surf (Input IN, inout SurfaceOutput o) 
		{
#include "./parallax_internals.cginc"
			fixed4 tex = tex2D(_MainTex, IN.uv_MainTex); 
			fixed4 spec = tex2D(_SpecularityMap, IN.uv_MainTex);
			fixed4 norm = tex2D(_BumpMap, IN.uv_BumpMap);
			o.Albedo = tex.rgb * _Color.rgb;
			#if defined(_VERTCOL_ON)
			o.Albedo *= IN.color;
			#endif
			o.Normal = UnpackNormal(norm);
			o.Emission.rgb = 0;
			o.Specular = spec.a;
			o.Gloss = Luminance(spec.rgb);
			o.Alpha = tex.a * _Color.a;
//			norm.x = 0;
//			norm.z = 0;
			//o.Normal = norm.xyz * 2 - 1;
//			o.SpecCol = spec.rgb * _SpecColor.rgb;
//		
//			o.AmbientOcc = tex2D(_AOHMap, IN.uv_MainTex).rgb;
//			
//			o.UV = IN.uv_MainTex;
		}
//		
//		inline float4 LightingMine(SurfaceOutputMine s, float3 lightDir, float3 viewDir, float atten)
//		{
//			half3 h = normalize (lightDir + viewDir);
//			
//			float diff = dot (s.Normal, normalize(lightDir));
//			float nh = max (0, dot (s.Normal, h));
//			#if defined(_LIGHTRAMPS_ON)
//			diff = tex2D(_DiffuseRamp, float2(diff,0.5f)).a;
//			nh = tex2D(_SpecularRamp, float2(nh,0.5f)).a;
//			#endif
//			//float ang = acos(dot(h,s.Normal));
//			float spec = pow (nh, s.Specular*128.0);
//			//float spec = exp(-pow(ang/s.Specular,2)) * s.Gloss;
//				
//			
//			float paraShadow = 0;
//#include "JDGLibShaderCommon/parallax_shadow_internals.cginc"
//			paraShadow = min(1, 1-paraShadow * _OcculusionShadowStrength);
//			
//			float invDiff = 1-diff;
//			float3 aoMul = clamp(1-(1-s.AmbientOcc)*_AOStrength*invDiff,0,1);
//						
//			float3 diffuseRes = s.Albedo * _LightColor0.rgb * diff * aoMul * paraShadow;
//			float3 specRes = _LightColor0.rgb * s.SpecCol * spec * paraShadow;
//			
//			float4 c;
//			c.rgb = (diffuseRes + specRes) * (atten * 2);//+ s.Albedo * tex2D(_EmissMap, s.UV).a;
//			//c.rgb = aoMul;//s.Alpha;//aoMul;//paraShadow;//s.AmbientOcc;//paraShadow;//* ;
//			//c.a = s.Alpha;// + _LightColor0.a * _SpecColor.a * spec * atten;
//			return c;
//		}
		ENDCG
	} 
	FallBack "Diffuse"
    CustomEditor "AllTheMapsMaterialInspector"
}
