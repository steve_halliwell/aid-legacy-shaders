﻿
//max determines number of steps in sample and perf. Higher values will also allow larger parallax heights
#define MIN_PARA_LINEAR_SAMPLES 10.0f
#define MAX_PARA_LINEAR_SAMPLES 40.0f
#define MAX_PARA_BINARY_SAMPLES 8
#define MAX_PARA_SCALE 0.3f	//set in material inspector range
//offset limiting will flatten things out at steep angles to avoid infinite samples
#define MAX_OFFSET 0.5f

//TODO
	
#ifdef _PARA_UNITY
	half h = tex2D (_AOHMap, IN.uv_BumpMap).a;
	float2 offset = ParallaxOffset (h, _Parallax, IN.viewDir);
	IN.uv_MainTex += offset;
	IN.uv_BumpMap += offset;
#endif


#if defined(_PARA_OFFSET)
	//mixture of AMD rendermonkey parallax offset mapping and Steep parallax mapping http://www.chandlerprall.com/2011/09/steep-parallax-shader/
	
	float2 dx = ddx( IN.uv_MainTex );
	float2 dy = ddy( IN.uv_MainTex );

	float2 offset = float2(0,0);
	float bumpScale = _Parallax;
	
	float3 tsE = -IN.viewDir;
	float bias = tsE.z;
	//also bias by how deep the effect is
	const float maxParallaxScale = MAX_PARA_SCALE;
	bias *= bumpScale / maxParallaxScale;
	bias = clamp(bias,0.0f,1.0f);
	//bias *= bias;
	
	
	
	//const float numSteps = 30.0; // How many steps the UV ray tracing should take
    int numSteps = lerp( MAX_PARA_LINEAR_SAMPLES*_OffsetQualityScale, MIN_PARA_LINEAR_SAMPLES*_OffsetQualityScale,  bias);
    //const int numSteps = 50;
    float height = 1.0;
    float step = 1.0 / numSteps;
    
    offset = IN.uv_BumpMap;
    float heightSample = tex2Dgrad(_AOHMap, offset,dx,dy).a;
 
 	//orig tse is neg x n y
    float2 delta = float2(-tsE.x, -tsE.y) * bumpScale / (tsE.z * numSteps);
	
	// offset limiting
	delta = normalize(delta) * clamp(length(delta),-MAX_OFFSET, MAX_OFFSET);
    
	int i = 0;

  	//moves linearly throu height field until it tips below
    for ( i = 0; i < numSteps; i++) {
        offset += delta;
        height -= step;
        heightSample = tex2Dgrad(_AOHMap, offset,dx,dy).a;
        
        if (heightSample > height) 
        {
        	break;
        }
    }
    
    //for binary move back to previous step and look halfway between iteratively
    height += step;
    offset -= delta;
    for( i = 0; i < MAX_PARA_BINARY_SAMPLES*_OffsetQualityScale; i++)
    {
    	delta /= 2;
    	step /= 2;
        offset += delta;
        height -= step;
        heightSample = tex2Dgrad(_AOHMap, offset,dx,dy).a;
        //prevH = NB.a;
        
        //if we over step move back and half search dist
        if(heightSample > height)
        {
	        offset -= delta;
	        height += step;
        }
    }
            
#if defined(_OFFSET_CLAMP_ON)
    if(offset.x < 0 || offset.x > 1 || offset.y < 0 || offset.y > 1)
   		discard;
#endif

	float2 uvdiff = offset - IN.uv_MainTex;
	
	IN.uv_MainTex += uvdiff;
	IN.uv_BumpMap += uvdiff;
#endif